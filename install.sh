#!/bin/bash
PARAM_1=$1
PARAM_2=$2
ROOT_UID=0
E_NOTROOT=67
KEY=(username passwd internet intranet nat proxy feedback port)
source ut.sh

########################
##### user check  ######
########################

chkUsr() {
    if [ $UID != $ROOT_UID ];then
        echo "Must be root to install mpush"
        exit $E_NOTROOT
    fi
}


initInstallList() {
    SERVICE_LIST=$(readIni $CONFIG install list)
    if [ ${#SERVICE_LIST[@]} = 0 ];then
        echo "no service to install"
        exit $E_FAIL
    fi
}

# initKeyList

initKeyList() {

   APNS_KEY=(${KEY[@]})
   GATEWAY_KEY=(${KEY[@]})
   CONNECTOR_KEY=(${KEY[@]})

   DATA_KEY=(${KEY[@]})

   unset -v APNS_KEY[7]
   unset -v APNS_KEY[5]
   unset -v APNS_KEY[2]

   unset -v GATEWAY_KEY[7]
   unset -v GATEWAY_KEY[6]

   unset -v CONNECTOR_KEY[5]

   unset -v DATA_KEY[6]
   unset -v DATA_KEY[5]
   unset -v DATA_KEY[2]

   echo ${APNS_KEY[@]}
   echo ${GATEWAY_KEY[@]}
   echo ${CONNECTOR_KEY[@]}
   echo ${DATA_KEY[@]}

}

# readServerByName [ServerName]

readServerByName() {
    if [ -z $1 ];then
        echo "[error] readServerByName(): no found param serverName"
        return
    fi


}

# service [service]
service() {

    if [ -z $1 ];then
        echo "[error] service(): service name cannot be null"
    fi

     case $1 in
     apns)
        local l=(${APNS_LIST[@]})
        local k=(${APNS_KEY[@]})
     ;;
     gateway)
        local l=(${GATEWAY_LIST[@]})
        local k=(${GATEWAY_KEY[@]})
     ;;
     connector)
        local l=(${CONNCETOR_LIST[@]})
        local k=(${CONNECTOR_KEY[@]})
     ;;
     data)
        local l=(${DATA_LIST[@]})
        local k=(${DATA_KEY[@]})
     ;;
     esac

    if [ ${#l[@]} = 0 ];then
        echo "[error] service(): server list is null"
    fi
    if [ ${#l[@]} = 0 ];then
        echo "[error] service(): service key is null"
    fi

    for server in ${l[@]};do
        local err_num=0
        echo "[info] $server starting..."
        local nat=$(readIni $CONFIG $server nat)
        if [ -z $nat ];then
            echo "[error] service(): read $server nat ip fail" | tee -a report
            err_num=`expr $err_num + 1`
            continue
        elif local res=$(chkIP $nat); test "${res}" = "" ;then
            echo "[error] service(): ping ${server} natIP[${nat}] fail from ${LOCAL_HOST_IP}" | tee -a report
            err_num=`expr $err_num + 1`
        fi



        ######################################################################################
        #upload system public key file /root/.ssh/mpush/id_rsa.pub to server:/root/.ssh/
        local flag=0
        for natip in ${HOST_NAT_LIST[@]};do
            if [ $nat = $natip ];then
                flag=`expr $flag + 1`
            fi
        done
        #echo ${SYS_PUB_KEY}
        if [ $flag -eq 0 ];then
            ssh root@${nat} 'echo '"${SYS_PUB_KEY}"' >> /root/.ssh/authorized_keys'
            if [ $? != 0 ];then
                echo "[error] service(): upload system public key to ${server} fail" | tee -a report
                err_num=`expr $err_num + 1`
            fi
            maintainHOST_NAT_LIST $nat

            scp serviceconfig.ini root@${nat}:/opt
            if [ $? != 0 ];then
                echo "[error] service(): upload serviceconfig.ini to ${server} fail" | tee -a report
                err_num=`expr $err_num + 1`
            fi

            scp ut.sh root@${nat}:/opt
            if [ $? != 0 ];then
                echo "[error] service(): upload util.sh to ${server} fail" | tee -a report
                err_num=`expr $err_num + 1`
            fi
        fi
        ######################################################################################


        ######################################################################################
        #upload start_${service}.sh ${service}.tar.gz  chk_${service}.sh to server
        if scp ${basicpath}/${1}.tar.gz root@${nat}:/opt ; [ $? != 0 ];then
            echo "[error] service(): upload ${basicpath}/${1}.tar.gz to ${server} fail" | tee -a report
            err_num=`expr $err_num + 1`
        fi
        if scp ${basicpath}/start_${1}.sh root@${nat}:/opt ; [ $? != 0 ];then
            echo "[error] service(): upload ${basicpath}/start_${1}.sh to ${server} fail" | tee -a report
            err_num=`expr $err_num + 1`
        fi
        if scp ${basicpath}/chk_${1}.sh root@${nat}:/opt ; [ $? != 0 ];then
            echo "[error] service(): upload ${basicpath}/chk_${1}.sh to ${server} fail" | tee -a report
            err_num=`expr $err_num + 1`
        fi
        ######################################################################################


        ######################################################################################
        #to start service on server
        local shell=start_${1}.sh
        ssh root@${nat} 'chmod +x '"/opt/${shell}"'; cd /opt; ./'"${shell}"''

        ######################################################################################

        if [ $err_num -eq 0 ];then
            echo "[info] $server start success"
        fi
        echo "[info] $server start end. total ${err_num} problems"
    done

}

#chkService [service]
chkService() {
   if [ -z ${1} ];then
        echo "[error] chkService(): service name cannot be null"
    fi

     case $1 in
     apns)
        local l=(${APNS_LIST[@]})
     ;;
     gateway)
        local l=(${GATEWAY_LIST[@]})
     ;;
     connector)
        local l=(${CONNCETOR_LIST[@]})
     ;;
     data)
        local l=(${DATA_LIST[@]})
     ;;
     esac

    if [ ${#l[@]} = 0 ];then
        echo "[error] service(): server list is null"
    fi

    for server in ${l[@]};do
        echo "[info] $server checking..."
        local nat=$(readIni $CONFIG $server nat)
        if [ -z $nat ];then
            echo "[error] service(): read $server nat ip fail" | tee -a report
            continue
        elif local res=$(chkIP $nat); test "${res}" = "" ;then
            echo "[error] service(): ping ${server} natIP[${nat}] fail from ${LOCAL_HOST_IP}" | tee -a report
        fi
        ######################################################################################
        #to start service on server
        local shell=chk_"${1}".sh
        ssh root@${nat} 'chmod +x '"/opt/${shell}"'; cd /opt; ./'"${shell} ${server}"';'

        ######################################################################################

    done

}

chkAllService() {

    for service in ${SERVICE_LIST[@]};do
        echo "[info] ----------- start check ${service} -----------"
        chkService ${service}
        echo "#######################################################"
    done

}


serviceAll() {

    for service in ${SERVICE_LIST[@]};do
        echo "[info] ----------- start install ${service} -----------"
        service $service
        echo "#######################################################"
    done

}

chkUsr
initInstallList
chkUploadFile
initSysPubKey
initKeyList

APNS_LIST=$(readIni $CONFIG apns list)
GATEWAY_LIST=$(readIni $CONFIG gateway list)
CONNCETOR_LIST=$(readIni $CONFIG connector list)
DATA_LIST=$(readIni $CONFIG data list)

case "$PARAM_1" in
    "chk")
    if [ "$PARAM_2" = "" ];then
        chkAllService
    else
        chkService $PARAM_2
    fi
    ;;
    "install")
    serviceAll
    ;;
    "")
    serviceAll
    chkAllService
esac

