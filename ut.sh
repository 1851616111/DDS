#!/bin/bash
E_SUCCESS=0
E_FAIL=1
E_NOT_IN_ONE_NET=3
E_NOPARAM=128
TELNET_TIME_OUT=5
PING_TIME_OUT=10
PING_COUNT=10
SSH_KEY_PATH=/root/.ssh
SSH_KEY_PASSWD=123456
INSTALL_CONFIG=install.ini
CONFIG=serviceconfig.ini
LOCAL_HOST_IP=`ifconfig eth0 | grep "inet addr" | cut -d ":" -f 2 | cut -d " " -f 1`
declare -a HOST_NAT_LIST=()

# chkIP [ip]
chkIP() {
    if [ -z $1 ];then
        echo "[error] chkIP(): no found param ip"
        return $E_NOPARAM
    fi
    if [ $1 = "localhost" -o $1 = "127.0.0.1" -o $1 = "${LOCAL_HOST_IP}" ];then
        return $E_SUCCESS
    fi

    local ret=`ping $1 -c $PING_COUNT -W $PING_TIME_OUT | tail -n 2`
    local t=`echo "$ret" | tail -n 1 | grep rtt`

    if [ "${t}" = "" ];then
        return $E_NOT_IN_ONE_NET
    else
        echo "${ret}"
        return $E_SUCCESS
    fi
}

# chkPort [ip] [port]
chkPort() {
    if [ -z $1 ];then
        echo "[error] chkPort(): find no ip and port"
        return $E_NOPARAM
    fi
    if [ -z $2 ];then
        echo "[error] chkPort(): find no ip or port"
        return $E_NOPARAM
    fi

    if [ $1 = "localhost" -o $1 = "127.0.0.1" -o $1 = "${LOCAL_HOST_IP}" ];then
        return $E_SUCCESS
    fi

    local ret=`nc -w $TELNET_TIME_OUT $1 $2 -v 2>nc.tmp ; cat nc.tmp`

    local l_succeeded=$(echo $ret | grep "succeeded")
    local l_failed=$(echo $ret | grep "failed")
    local l_timeout=$(echo $ret | grep "timed")

    rm nc.tmp

    if [ ! -z "${l_succeeded}" ];then
        return $E_SUCCESS
    elif [ ! -z "${l_failed}" -o ! -z "${l_timeout}"  ];then
        return $E_FAIL
    fi

}
# maintainHOST_NAT_LIST [nat ip]

maintainHOST_NAT_LIST() {
    if [ "$1" = "" ];then
        return $E_NOPARAM
    fi
    for ip in ${HOST_NAT_LIST[@]}
    do
        if [ $ip = $1 ];then
            return $E_SUCCESS
        fi
    done
    HOST_NAT_LIST+=($1)
}

# serviceChk [ip] [port]

serviceChk() {

    if ret=`chkIP $1`; test $? -eq 0; then
       if chkPort $1 $2; test $? -eq 0;then
            return $E_SUCCESS
        fi
    fi
    return $E_FAIL

}

# getSysPubKey

initSysPubKey() {
    if [ ! -f ${SSH_KEY_PATH}/id_rsa.pub ];then
         ssh-keygen -t rsa -f ${SSH_KEY_PATH}/id_rsa 1>/dev/null

    fi
        SYS_PUB_KEY=$(cat $SSH_KEY_PATH/id_rsa.pub)
    if [ $? = 0 ];then
        return $E_SUCCESS
    else
        echo "[error] ut.sh --> getSysPubKey(): generate public key fail"
        return $E_FAIL
    fi
}


# readIni [配置文件路径+名称] [节点名] [键值]

readIni() {
    local FILE=$1
    local SECTION=$2
    local KEY=$3
    if [ -z $FILE -o -z $SECTION -o -z $KEY ];then
        echo "readIni: can not found param"
    fi
    if chkFile $1;then
        list=`awk -F '=' '/\['${SECTION}'\]/{a=1}a==1&&$1~/'${KEY}'/{print $2;exit}' ${FILE}`
        echo ${list//,/ }
    fi
}

# chkFIle [文件路径+名称]

chkFile() {
    if [ ! -f $1 ];then
        return $E_FAIL
    fi
    return $E_SUCCESS
}

# chkFileByService [service name]
chkFileByService() {
    if [ -z $1 ];then
        echo "[error] ut.chkFileByService(): no found param " | tee -a report

    fi

    #service binary check
    if [  ! -f upload/${1}/${1} ];then
        echo "[error] ut.chkFileByService(): file upload/${1}/${1} not exists" | tee -a report

    fi
    #service restart.sh check
    if [ ! -f upload/${1}/restart.sh ];then
        echo "[error] ut.chkFileByService(): file upload/${1}/restart.sh not exists" | tee -a report

    fi

    if [ $1 = "gateway" ];then
        if [ ! -f ./upload/${1}/conf.json ];then
            echo "[error] ut.chkFileByService():  file upload/${1}/conf.json not exists" | tee -a report

        fi
        if [ ! -d ./upload/${1}/mpush_std ];then
            echo "[error] ut.chkFileByService():  file upload/${1}/conf.json not exists" | tee -a report

        fi
    fi
    return 0
}

chkUploadFile() {
    local root=root
    basicpath=$(readIni $INSTALL_CONFIG $root basicpath)
    file=$(readIni $INSTALL_CONFIG $root file)
    dir=$(readIni $INSTALL_CONFIG $root dir)

    local flag=0

    for f in ${file[@]};do
        if [ ! -f ${basicpath}/${f} ];then
            flag=`expr flag + 1`
            echo "[error] ut.chkUploadFile:  ${basicpath}/${f} not exits" | tee -a report
        fi
    done

    for d in ${dir[@]};do
        local flist=$(readIni $INSTALL_CONFIG $d name)
        for f in ${flist[@]};do
            if [ ! -e ${basicpath}/${d}/${f} ];then
                flag=`expr flag + 1`
                echo "[error] ut.chkUploadFile:  ${basicpath}/${d}/${f} not exits" | tee -a report
            fi
        done
    done

    if [ $flag != 0 ];then
        exit
    fi

    cd ${basicpath}
    for d in ${dir[@]};do
        if [ -e ${d}.tar.gz ];then
            rm ${d}.tar.gz -f
        fi
        tar -czvf ${d}.tar.gz ${d}
    done
    cd ..
}

#chkServer [dst server name] [dst ip] [dst port] [src name] [src ip]
chkServer() {
    local r=$(chkPort ${2} ${3})
    case $? in
        ${E_SUCCESS})
            echo -e "\e[1;32m [telnet] ${1}[${2}] ${3} success from ${4}[${5}] \e[0m"
        ;;
        ${E_FAIL})
            local res=$(chkIP ${2})
            if [ "${res}" != "" ];then
                echo -e "\e[1;31m [telnet] ${1}[${2}] ${3} fail from ${4}[${5}] , for port $port close \e[0m"
            else
                echo -e "\e[1;31m [telnet] ${1}[${2}] ${3} fail from ${4}[${5}] , for unknown reason(net or port) \e[0m"
            fi
        ;;
    esac

    local res2=$(chkIP ${2})
    if [ "${res2}" != "" ];then
        echo -e "\e[1;32m [ping] $2 from ${4}[${5}] success \e[0m"
        echo "${res2}"
    else
        echo -e "\e[1;43m [ping] $2 from ${4}[${5}] fail. maybe the net manager do not allow ping protocal \e[0m"
    fi

}