#!/bin/bash
INSTALL_PATH=mpush
SERVICE=apns
source ut.sh

echo "start $SERVICE"
# getPID() [server]
getPID() {
    if [ "$1" != "" ];then
        pid=$(netstat -nptl | grep "./${1}" | awk '{print $7}' | cut -d "/" -f 1)
    fi
    echo $pid
}

chkNohup() {
    for((i=1;i<=3;i++));do
        if [ ! -f /opt/${INSTALL_PATH}/${SERVICE}/nohup.out ];then
            sleep 3
        fi
    done
    ret=$(cat /opt/${INSTALL_PATH}/${SERVICE}/nohup.out | grep "start gateway on :7000")
    if [ "$ret" != "" ];then
        return 0
    fi
    return 1
}

chkNetstat() {
    ret=$(netstat -nptl | grep ":7000")
    if [ "$ret" != "" ];then
        return 0
    fi
    return 1
}

chkService() {
    if  chkNohup;[ $? = 0 ]; chkNetstat; [ $? = 0 ];then
        echo "apns start success "
        return 0
    fi
    echo "apns start fail "
    return 1
}

start() {
    if [ ! -d $INSTALL_PATH ];then
        mkdir $INSTALL_PATH -p
    fi

    if [ -d ${INSTALL_PATH}/${SERVICE} ];then
        rm ${INSTALL_PATH}/${SERVICE} -rf
    fi


    if r=$(tar -zxvf ${SERVICE}.tar.gz -C ${INSTALL_PATH}  2>&1); [ $? != 0 ];then
        rr_e=$(echo $r | grep "time stamp")
        rr_c=$(echo $r | grep "时间戳")
        if [ "${rr_e}" != "" -o "${rr_c}" != "" ];then
            echo "[error] tar -zxvf ${SERVICE}.tar.gz -C ${INSTALL_PATH}"
        fi
    fi
    sleep 5  #tar need time


    chmod u+x ${INSTALL_PATH}/${SERVICE}/restart.sh
    old_pid=$(getPID ${SERVICE})

    if [ "$old_pid" !=  "" ];then
        kill -9 $old_pid
    fi

    cd mpush/${SERVICE}
    echo "> cd /opt/${INSTALL_PATH}/${SERVICE}"

    ./restart.sh
    echo "> ./restart.sh"

    sleep 5 #generate nohup need time
}

start
chkService

