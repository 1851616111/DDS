#!/bin/bash
INSTALL_PATH=mpush
SERVICE=apns
SERVER=${1}
source ut.sh
NET_TASK=$(readIni $CONFIG apple_servers list)

init() {
    if [ "${SERVER}" = "" ];then
        echo "[error] ${0} find no param server"
    fi
}

chkNET_TASH() {
    if [ ${#NET_TASK[@]} = 0 ];then
        echo "[error] read ${CONFIG} apple_servers fail"
    fi
}

chkNet() {
    # apple 4 aps ip
    for i in ${NET_TASK[@]};do
        local ip=$(echo ${i} | cut -d ":" -f 1)
        local port=$(echo ${i} | cut -d ":" -f 2)

        chkPort ${ip} ${port}
        case $? in
        ${E_SUCCESS})
            echo -e "\e[1;32m [telnet] $ip $port success from ${SERVER}[${LOCAL_HOST_IP}] \e[0m"
        ;;
        ${E_FAIL})
            local res_=$(chkIP ${ip})
            if [ "${res}" != "" ];then
                echo -e "\e[1;31m [telnet] $ip $port fail from ${SERVER}[${LOCAL_HOST_IP}] , for port $port close \e[0m"
            else
                echo -e "\e[1;31m [telnet] $ip $port fail from ${SERVER}[${LOCAL_HOST_IP}] , for unknown reason(net or port) \e[0m"
            fi
        ;;
        esac

        #delay detail
        local res_=$(chkIP ${ip})
        if [ "${res}" != "" ];then
            echo -e "\e[1;32m [ping] $ip from ${SERVER} ${LOCAL_HOST_IP} success \n  ${res} \e[0m"
            echo  " ${res} "
        else
            echo -e "\e[1;43m [ping] $ip from ${SERVER} ${LOCAL_HOST_IP} fail. maybe the net manager not allow ping protocal \e[0m"
        fi

    done

    #gateway feedback check feedback can be proxy
    local feedback=$(readIni $CONFIG $SERVER feedback)
    local r=$(chkPort ${ip} ${port})
    case $? in
        ${E_SUCCESS})
            echo -e "\e[1;32m [telnet] gateway feedback ${feedback} 3000 success from ${SERVER}[${LOCAL_HOST_IP}] \e[0m"
        ;;
        ${E_FAIL})
            local res=$(chkIP ${ip})
            if [ "${res}" != "" ];then
                echo -e "\e[1;31m [telnet] feedback gateway[${feedback}] 3000 fail from ${SERVER}[${LOCAL_HOST_IP}] , for port $port close \e[0m"
            else
                echo -e "\e[1;31m [telnet] feedback gateway[${feedback}] 3000 fail from ${SERVER}[${LOCAL_HOST_IP}] , for unknown reason(net or port) \e[0m"
            fi
        ;;
    esac

    local res2=$(chkIP ${ip})
    if [ "${res2}" != "" ];then
        echo -e "\e[1;32m [ping] $ip from ${SERVER}[${LOCAL_HOST_IP}] success \e[0m"
        echo "${res2}"
    else
        echo -e "\e[1;43m [ping] $ip from ${SERVER}[${LOCAL_HOST_IP}] fail. maybe the net manager do not open ping protocal \e[0m"
    fi
}

init
chkNET_TASH
chkNet
