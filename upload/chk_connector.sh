#!/bin/bash
INSTALL_PATH=mpush
SERVICE=connector
SERVER=${1}
source ut.sh

init() {
    if [ "${SERVER}" = "" ];then
        echo "[error] ${0} find no param server"
    fi
}

initChkList() {
    CHK_TASK_LIST=$(readIni $CONFIG chk_${SERVICE} list)
    if [ ${#CHK_TASK_LIST[@]} = 0 ];then
        echo "[error] check $SERVER task list is 0"
    fi
}

chkNet() {
    task_server_list=()
    for task in ${CHK_TASK_LIST[@]};do
        if [  "$task" != "" ];then
            local task_tp=$(echo $task | cut -d "." -f 1)
            local task_name=$(echo $task | cut -d "." -f 2)
            local key1=$(echo $task | cut -d "." -f 3)
            local key2=$(echo $task | cut -d "." -f 4)

            if [ "$task_name" = "self" ];then
                task_name=$SERVER
            fi

            case ${task_tp} in
                "server")
                    task_server_list+=($task_name)
                ;;
                "service")
                    #if [ "$task_name" = "mongodb" ];then
                     #   local s=$(readIni $CONFIG $task_name list)
                    #fi
                    local l=$(readIni $CONFIG $task_name list)
                    task_server_list+=(${l[@]})
                ;;
            esac

            for s in ${task_server_list[@]};do
                local key1_value=$(readIni $CONFIG $s $key1)
                if [ "${key2}" = "port" ];then
                   local key2_value=$(readIni $CONFIG $s $key2)
                else
                   local key2_value=$key2
                fi
                #chkServer [dst server name] [dst ip] [dst port] [src name] [src ip]
                echo "1:$s 2:$key1_value 3:$key2_value 4:$SERVER 5:$LOCAL_HOST_IP"
                chkServer $s $key1_value $key2_value $SERVER $LOCAL_HOST_IP

            done
        fi
    done

}

init
initChkList
chkNet
