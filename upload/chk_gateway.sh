#!/bin/bash
INSTALL_PATH=mpush
SERVICE=gateway
SERVER=$1
source ut.sh



chkNet() {
    # apple 4 aps ip
    for i in ${NET_TASK[@]};do
        local ip=$(echo ${i} | cut -d ":" -f 1)
        local port=$(echo ${i} | cut -d ":" -f 2)
        local r=$(chkPort ${ip} ${port})

        case $? in
        ${E_SUCCESS})
            echo -e "\e[1;32m [telnet]  $ip $port success from ${SERVER} ${LOCAL_HOST_IP} \e[0m"
        ;;
        ${E_FAIL_PORT_CLOSE})
            echo -e "\e[1;31m [telnet]  $ip $port fail from ${SERVER} ${LOCAL_HOST_IP} , for port $port close \e[0m"
        ;;
        ${E_NOT_IN_ONE_NET})
            echo "\e[1;31m [telnet]  $ip $port fail from ${SERVER} ${LOCAL_HOST_IP} , for not in one network \e[0m"
        ;;
        esac

        local res_=$(chkIP ${ip})
        if [ "${res}" != "" ];then
            echo -e "\e[1;32m [ping] $ip from ${SERVER} ${LOCAL_HOST_IP} success \n  ${res} \e[0m"
            echo  " ${res} "
        else
            echo -e "\e[1;43m [ping] $ip from ${SERVER} ${LOCAL_HOST_IP} fail. maybe the net manager do not open ping protocal \e[0m"
        fi

    done

    #gateway feedback check feedback can be proxy
    local feedback=$(readIni $CONFIG $SERVER feedback)
    local r=$(chkPort ${ip} ${port})
    case $? in
        ${E_SUCCESS})
            echo -e "\e[1;32m [telnet] gateway feedback ${feedback} 3000 success from ${SERVER} ${LOCAL_HOST_IP} \e[0m"
        ;;
        ${E_FAIL_PORT_CLOSE})
            echo -e "\e[1;31m [telnet] gateway feedback ${feedback} 3000 fail from ${SERVER} ${LOCAL_HOST_IP} , for port $port close \e[0m"
        ;;
        ${E_NOT_IN_ONE_NET})
            echo "\e[1;31m [telnet] gateway feedback ${feedback} 3000 fail from ${SERVER} ${LOCAL_HOST_IP} , for not in one network \e[0m"
        ;;
    esac

    local res2=$(chkIP ${ip})
    if [ "${res2}" != "" ];then
        echo -e "\e[1;32m [ping] $ip from ${SERVER} ${LOCAL_HOST_IP} success \e[0m"
        echo "${res2}"
    else
        echo -e "\e[1;43m [ping] $ip from ${SERVER} ${LOCAL_HOST_IP} fail. maybe the net manager do not open ping protocal \e[0m"
    fi
}

chkNet
